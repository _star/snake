/* eslint-disable no-nested-ternary */
/* eslint-disable max-classes-per-file */
import { List } from 'immutable';
import {
  DIRECTIONS, findDirection, directionVec2, reverseDirection,
} from './Direction';
import { choice } from './Random';

class Snake {
  constructor(initPos, maxBound) {
    this.oriented = choice(DIRECTIONS);
    const rev = reverseDirection(this.oriented);
    const v = directionVec2(rev);
    const secondPos = initPos.add(v);
    this.internal = new List([initPos, secondPos, secondPos.add(v)]);
    this.maxBound = maxBound;
  }

  move(dir) {
    const d = directionVec2(dir);
    const head = this.internal.first().add(d);

    const { x, y } = head;
    const { x: x0, y: y0 } = this.maxBound;
    head.x = x < 0 ? x0
      : x >= x0 ? 0 : x;
    head.y = y < 0 ? y0
      : y >= y0 ? 0 : y;

    this.internal = this.internal.pop();
    const res = !this.internal.includes(head);
    this.internal = this.internal.unshift(head);
    return res;
  }

  grow(size = 1) {
    let tail = this.internal.last();
    const secondTail = this.internal.get(-2);
    try {
      const dir = findDirection(tail, secondTail);
      const d = directionVec2(dir);
      for (let i = 0; i < size; i++) {
        tail = tail.add(d);
        this.internal = this.internal.push(tail);
      }
    } finally {
      // ignored
    }
  }

  forward() {
    return this.move(this.oriented);
  }
}

export default Snake;
