import Color from './Color';
import Vec2 from './Vec2';

export default class Food {
  constructor(center, radius, color) {
    this.center = center;
    this.radius = radius;
    this.color = color;
  }

  static random() {
    const food = new Food(new Vec2(Math.random(), Math.random()), Math.random(), Color.random());
    return food;
  }
}
