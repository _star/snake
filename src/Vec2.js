class Vec2 {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  hashCode() {
    return (this.x << 16) & this.y;
  }

  equals(other) {
    return this.x === other.x && this.y === other.y;
  }

  add(vec2) {
    return new Vec2(this.x + vec2.x, this.y + vec2.y);
  }

  mul(factor) {
    return new Vec2(this.x * factor, this.y * factor);
  }
}

export default Vec2;
