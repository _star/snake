import Vec2 from './Vec2';

/* eslint-disable no-nested-ternary */
const UP = 'UP';
const DOWN = 'DOWN';
const LEFT = 'LEFT';
const RIGHT = 'RIGHT';
const DIRECTIONS = [UP, DOWN, LEFT, RIGHT];

class UnknownDirectionException extends Error {
  constructor(message = '') {
    if (message !== '') {
      super(`Unknow Direction ${message}`);
    } else {
      super('Unknow Direction');
    }
  }
}

function reverseDirection(dir) {
  switch (dir) {
    case UP:
      return DOWN;
    case DOWN:
      return UP;
    case LEFT:
      return RIGHT;
    case RIGHT:
      return LEFT;
    default:
      throw new UnknownDirectionException();
  }
}

function findDirection({ x: x0, y: y0 }, { x: x1, y: y1 }) {
  // console.log(`${x0} ${y0} ${x1} ${y1}`);
  const dx = x0 - x1;
  const dy = y0 - y1;
  console.log(`${dx} ${dy} ${x0} ${y0} ${x1} ${y1}`);
  if (dx === 0 && dy === 0) {
    throw new UnknownDirectionException();
  }

  return dx === 1 ? RIGHT
    : dx === -1 ? LEFT
      : dy === 1 ? DOWN
        : UP;
}

const DirectionsVec2 = [new Vec2(0, -1), new Vec2(0, 1), new Vec2(-1, 0), new Vec2(1, 0)];

function directionVec2(dir) {
  switch (dir) {
    case UP:
      return DirectionsVec2[0];
    case DOWN:
      return DirectionsVec2[1];
    case LEFT:
      return DirectionsVec2[2];
    case RIGHT:
      return DirectionsVec2[3];
    default:
      throw new UnknownDirectionException();
  }
}

export {
  reverseDirection, findDirection, directionVec2,
  UP, DOWN, LEFT, RIGHT, DIRECTIONS, UnknownDirectionException,
};
