function randomInt(low, high) {
  const bound = high - low;
  return Math.floor(Math.random() * bound + low);
}

function choice(list) {
  const { length } = list;
  const index = randomInt(0, length);
  return list[index];
}


export {
  randomInt,
  choice,
};
