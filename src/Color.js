import { randomInt } from './Random';

export default class Color {
  constructor(r, g, b) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  static random() {
    return new Color(randomInt(0, 256), randomInt(0, 256), randomInt(0, 256));
  }

  toHexString() {
    return `#${this.r.toString(16)}${this.g.toString(16)}${this.b.toString(16)}`;
  }

  toRGBString() {
    return `rgb(${this.r},${this.g},${this.b})`;
  }
}
